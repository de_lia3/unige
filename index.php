<!--
  Delia Sciretta - 16.11.2021
-->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Exercice Unige</title>
    <link rel="stylesheet" href="style.css">
    <script src="./pluging/jquery-3.6.0.min.js" ></script>
    <link rel="stylesheet" href="./pluging/bootstrap/css/bootstrap.min.css" >
    <script src="./pluging/bootstrap/js/bootstrap.min.js" ></script>
    <?php require_once "./php/fichier_connexion.php"; ?>
  </head>

  <body>
    <?php
      $idPok = 0;
      if (isset($_GET['idPokemon'])){
        $idPok = $_GET['idPokemon'];
      }
    ?>
    <main>
      <div class="containerPersonalized">
        <div class="row" id="details" style="height:60vh">
          <div class="col-4" id="imgDetail"></div>
          <div class="col-8" id="descrDetail"></div>
        </div>
        <div class="row" id="tabPokemon" style="height:38vh">
          <div class="col-12">
            <table class="table table-hover table-borderless" style="width:100%">
              <thead >
                <tr>
                  <th style="border-bottom: none;" scope="col">Index</th>
                  <th scope="col"></th>
                  <th scope="col">Name</th>
                  <th scope="col">Catch rate</th>
                  <th scope="col">Height</th>
                  <th scope="col">Weight</th>
                  <th scope="col">Type</th>
              </thead>
              <tbody>
                <?php
                   $rqtAllPokemon=$db->query("SELECT p.id, p.ndex, p.name, p.catch_rate, p.height, p.weight, p.image, p.description, GROUP_CONCAT(t.type SEPARATOR ',') AS typePokemon from pokemon p INNER JOIN pokemon_type pt ON p.id=pt.Id_Pokemon INNER JOIN type t ON pt.Id_Type = t.id GROUP BY p.id, p.name, p.ndex, p.catch_rate, p.height, p.weight, p.image, p.description ORDER BY ndex ASC");            
                   $rqtAllPokemon->execute();
                   while ($resAllPokemon = $rqtAllPokemon->fetch()) { ?>
                        <tr class="lineStyle" onClick={getDetailsPokemon(<?php echo $resAllPokemon['name'] ?>, <?php echo $resAllPokemon['description'] ?>)}>
                          <td>#<?php echo $resAllPokemon['ndex'] ?></td>
                          <?php echo '<td><img src="data:image/jpeg;base64,'.base64_encode($resAllPokemon['image']).'" alt="'.$resAllPokemon['name'].'" width=50/></td>'; ?>
                          <td style="width:30%"><?php echo $resAllPokemon['name'] ?></td>
                          <td><?php echo $resAllPokemon['catch_rate'] ?></td>
                          <td><?php echo $resAllPokemon['height'] ?> m</td>
                          <td><?php echo $resAllPokemon['weight'] ?> kg</td>
                          <td style="width:10%">
                            <?php 
                              $arrayType = explode(",", $resAllPokemon['typePokemon']);
                              for ($x = 0; $x < count($arrayType); $x++) {
                                echo '<span class="orangeType">'.$arrayType[$x].'</span>';
                              }  
                            ?>
                          </td>
                        </tr>
                    <?php
                   }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </main>

    <footer>
      <p>Delia Sciretta <small>16.11.2021</small></p>
    </footer>

    <script>
      function getDetailsPokemon(name, description){
        /* var img='<img src="data:image/jpeg;base64,'+base64_encode(image)+'" width=150/>'
        $('#imgDetail').html(img) */
        var affichage = "<h2>"+name+"</h2>";
        affichage += "<p>"+description+"</p>";
        $('#descrDetail').html(affichage)

        /* $.ajax({
            url:"./php/getDetailsPokemon.php",    //the page containing php script
            type: "post",    //request type,
            dataType: 'json',
            data: {vIdPokemon: id},
            success:function(result){
              
              var img='<img src="data:image/jpeg;base64,'+base64_encode($result['image'])+'" width=150/>'
              $('#imgDetail').html(img)
             
              var affichage = "<h2>"+$result['image']+"</h2>";
              affichage += "<p>"+$result['description']+"</p>";
              $('#details').html(affichage)
            }
        }); */
      }
    </script>
  </body>
</html>